﻿var ContactListController = {
    //urls
    handlerUrl: "/Service/ContactsCommandHandler.ashx",
    //commands
    getContactsCommand: "getContacts",
    saveContactCommand: "saveContact",
    removeContactCommand: "removeContact",
    findContactsCommand: "findContacts",
    //data
    localStorage: [],
    //methods
    Init: function () {
        ViewLogic.Init();
    },
    GetAllContacts: function (callback, failCallback) {
        $.getJSON(ContactListController.handlerUrl + "?cmd=" + ContactListController.getContactsCommand, function (contacts) {
            ContactListController.localStorage = contacts;
            callback(contacts);
        }, failCallback).fail(function () {
            failCallback();
        });
    },
    FindContacts: function (searchText, callback, failCallback) {
        $.ajax({
            type: "POST",
            url: ContactListController.handlerUrl + "?cmd=" + ContactListController.findContactsCommand,
            data: '{searchText:"' + searchText + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }).done(function (contacts) {
            ContactListController.localStorage = contacts;
            callback(contacts);
        }).fail(function () {
            failCallback();
        });
    },
    GetContactById: function (id) {
        for (var i = 0; i < ContactListController.localStorage.length; i++) {
            if (ContactListController.localStorage[i].Id == id) {
                return ContactListController.localStorage[i];
            }
        }
        return null;
    },
    RemoveContactById: function (id, callback, failCallback) {
        $.ajax({
            type: "POST",
            url: ContactListController.handlerUrl + "?cmd=" + ContactListController.removeContactCommand,
            data: '{contactId:"' + id + '"}',
            dataType: "json"
        }).done(function () {
            for (var i = 0; i < ContactListController.localStorage.length; i++) {
                if (ContactListController.localStorage[i].Id == id) {
                    ContactListController.localStorage.splice(i, 1);
                    break;
                }
            }
            callback();
        }).fail(function () {
            failCallback();
        });
    },
    SaveContact: function (contact, callback, failCallback) {
        $.ajax({
            type: "POST",
            url: ContactListController.handlerUrl + "?cmd=" + ContactListController.saveContactCommand,
            data: JSON.stringify(contact),
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }).done(function (contact) {
            callback(contact);
            for (var i = 0; i < ContactListController.localStorage.length; i++) {
                if (ContactListController.localStorage[i].Id == contact.Id) {
                    ContactListController.localStorage[i] = contact;
                    return;
                }
            }
            ContactListController.localStorage.push(contact);
        }).fail(function () {
            failCallback();
        });
    },
    ContactsLength: function () {
        return ContactListController.localStorage.length;
    }
}