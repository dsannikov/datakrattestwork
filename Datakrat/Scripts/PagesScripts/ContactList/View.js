﻿var ViewLogic = {
    contactTemplate: null,
    Init: function () {
        var source = $("#template-ContactTemplate").html();
        ViewLogic.contactTemplate = Handlebars.compile(source);
        LoadingScreen.Show();
        ContactListController.GetAllContacts(function (contacts) {
            ViewLogic.DisplayContacts(contacts);
            LoadingScreen.Hide();
        }, function () {
            alert("В системе произошла ошибка.");
            LoadingScreen.Hide();
        });
        $('#modal-contactList-addEditModal').on('shown.bs.modal', function () {
            $("#input-contactList-addEditModal-name").focus();
        });
        $("#button-contactList-addContact").click(function () {
            $("#label-contactList-addEditModal-title").html("Новый контакт");
            ViewLogic.cleanAddEditModal();
            $('#modal-contactList-addEditModal').modal('show');
        });
        $("#button-contactList-addEditModal-save").click(function () {
            if (jQuery.trim($("#input-contactList-addEditModal-name").val()).length == 0) {
                alert("Поле имя обязательно для заполнения");
                return;
            }
            LoadingScreen.Show();
            var resultContact = ViewLogic.prepareContactFromModal();
            ContactListController.SaveContact(resultContact, function (contact) {
                LoadingScreen.Hide();
                if (resultContact.contactId == '') {
                    ViewLogic.DisplayContacts([contact]);
                } else {
                    ViewLogic.ChangeContact(contact);
                }
            }, function () {
                alert("В системе произошла ошибка.");
                LoadingScreen.Hide();
            });
            $('#modal-contactList-addEditModal').modal('hide');
        });
        $("#container-contactList-list").on("click", ".button-contactList-list-contactItem-edit", function () {
            var id = $(this).data('id');
            var contact = ContactListController.GetContactById(id);
            if (contact != null) {
                ViewLogic.fillAddEditModal(contact);
                $("#label-contactList-addEditModal-title").html("Редактирование контакта");
                $('#modal-contactList-addEditModal').modal('show');
            } else {
                alert("Контакт не найден");
            }
        });
        $("#container-contactList-list").on("click", ".button-contactList-list-contactItem-remove", function () {
            var id = $(this).data('id');
            LoadingScreen.Show();
            ContactListController.RemoveContactById(id, function () {
                LoadingScreen.Hide();
                ViewLogic.RemoveContactById(id);
            }, function () {
                alert("В системе произошла ошибка.");
                LoadingScreen.Hide();
            });
        });
        $("#button-contactList-find").click(function () {
            var search = $("#input-contactList-searchText").val();
            LoadingScreen.Show();
            ContactListController.FindContacts(search, function (contacts) {
                ViewLogic.ClearTable();
                ViewLogic.DisplayContacts(contacts);
                LoadingScreen.Hide();
            }, function () {
                alert("В системе произошла ошибка.");
                LoadingScreen.Hide();
            });
        });
    },
    cleanAddEditModal: function () {
        $("#input-contactList-addEditModal-contactId").val("");
        $("#input-contactList-addEditModal-name").val("");
        $("#input-contactList-addEditModal-phone").val("");
        $("#input-contactList-addEditModal-email").val("");
        $("#textarea-contactList-addEditModal-notes").val("");
    },
    fillAddEditModal: function (contact) {
        $("#input-contactList-addEditModal-contactId").val(contact.Id);
        $("#input-contactList-addEditModal-name").val(contact.Name);
        $("#input-contactList-addEditModal-phone").val(contact.Phone);
        $("#input-contactList-addEditModal-email").val(contact.Email);
        $("#textarea-contactList-addEditModal-notes").val(contact.Notes);
    },
    prepareContactFromModal: function () {
        var contact = new Object();
        contact.contactId = $("#input-contactList-addEditModal-contactId").val();
        contact.name = $("#input-contactList-addEditModal-name").val();
        contact.phone = $("#input-contactList-addEditModal-phone").val();
        contact.email = $("#input-contactList-addEditModal-email").val();
        contact.notes = $("#textarea-contactList-addEditModal-notes").val();
        return contact;
    },
    DisplayContacts: function (contacts) {
        var resultString = "";
        for (var i = 0; i < contacts.length; i++) {
            var html = ViewLogic.contactTemplate(contacts[i]);
            resultString += html;
        }
        if (contacts.length > 0) {
            $("#container-contactList-list-emptyHolder").hide();
            $(resultString).prependTo("#container-contactList-list").hide().slideDown();
        } else {
            $("#container-contactList-list-emptyHolder").show();
        }
    },
    ChangeContact: function (contact) {
        var html = ViewLogic.contactTemplate(contact);
        $('#container-contactList-list-contactItem_' + contact.Id).fadeOut("slow", function () {
            var div = $(html).hide();
            $(this).replaceWith(div);
            $('#container-contactList-list-contactItem_' + contact.Id).fadeIn("slow");
        });
    },
    RemoveContactById: function (id) {
        $('#container-contactList-list-contactItem_' + id).fadeOut("slow", function () {
            $(this).remove();
            if (ContactListController.ContactsLength() == 0) {
                $("#container-contactList-list-emptyHolder").fadeIn("slow");
            }
        });
    },
    ClearTable: function () {
        $(".container-contactList-list-contactItem").remove();
    }
}