﻿<%@ Page Title="Список контактов" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contacts.aspx.cs" Inherits="Datakrat.Contacts" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <div class="row">
        <div class="col-sm-2">
            <button type="button" class="btn btn-success" id="button-contactList-addContact">Добавить</button>
        </div>
        <div class="col-sm-10">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Введите строку для поиска" id="input-contactList-searchText">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="button" id="button-contactList-find">Поиск</button>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Имя</th>
                        <th>Телефон</th>
                        <th>Email</th>
                        <th>Дата создания</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="container-contactList-list">
                    <tr id="container-contactList-list-emptyHolder">
                        <td colspan="5">В системе не найдено контактов</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modal-contactList-addEditModal" tabindex="-1" role="dialog" aria-labelledby="label-contactList-addEditModal-title">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="label-contactList-addEditModal-title">Новый контакт</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="input-contactList-addEditModal-contactId">
                    <div class="form-group">
                        <label for="input-contactList-addEditModal-name">Имя</label>
                        <input type="text" class="form-control" id="input-contactList-addEditModal-name" placeholder="Имя">
                    </div>
                    <div class="form-group">
                        <label for="input-contactList-addEditModal-phone">Телефон</label>
                        <input type="tel" class="form-control" id="input-contactList-addEditModal-phone" placeholder="Телефон">
                    </div>
                    <div class="form-group">
                        <label for="input-contactList-addEditModal-email">Email</label>
                        <input type="email" class="form-control" id="input-contactList-addEditModal-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="textarea-contactList-addEditModal-notes">Заметки</label>
                        <textarea class="form-control" id="textarea-contactList-addEditModal-notes" rows="3" placeholder="Заметки"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-primary" id="button-contactList-addEditModal-save">Сохраить</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="SciptsContent" ContentPlaceHolderID="ScriptsPlaceHolder" runat="server">
    <script id="template-ContactTemplate" type="text/x-handlebars-template">
        <tr id="container-contactList-list-contactItem_{{Id}}" class="container-contactList-list-contactItem">
            <td>{{Name}}
            </td>
            <td>{{Phone}}
            </td>
            <td>{{Email}}
            </td>
            <td>{{CreationDateTimeDisplay}}
            </td>
            <td>
                <div class="btn-group" role="group">
                    <button type="button" data-id="{{Id}}" class="button-contactList-list-contactItem-edit btn btn-info">Редактировать</button>
                    <button type="button" data-id="{{Id}}" class="button-contactList-list-contactItem-remove btn btn-danger">Удалить</button>
                </div>
            </td>
        </tr>
    </script>
    <script src="/Scripts/PagesScripts/ContactList/Controller.js" type="text/javascript"></script>
    <script src="/Scripts/PagesScripts/ContactList/View.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            ContactListController.Init();
        });
    </script>
</asp:Content>
