﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoadingScreen.ascx.cs" Inherits="Datakrat.Controls.LoadingScreen" %>
<aside id="loadingScreen" class="loading-screen" style="position: fixed; top: 0; right: 0; bottom: 0; left: 0; z-index: 9999; display: none" >
    <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4" style="position: absolute; top: 50%; margin-top: -7em; border: 1px solid black; background-color: white; -ms-border-radius: 5px; border-radius: 5px;">
        <div class="page-header">
            <i class="text-primary pull-left glyphicon glyphicon-refresh glyphicon-refresh-animate" style="-ms-text-shadow: 0px 0px 0ex; text-shadow: 0px 0px 0ex; font-size: 26px"></i>
            <hgroup class="" style="-ms-text-shadow: 0px 0px 0ex; text-shadow: 0px 0px 0ex;">
                <h3>&nbsp;&nbsp;Идёт загрузка<br>
                    <small>Пожалуйста подождите</small>
                </h3>
            </hgroup>
        </div>
    </div>
</aside>
