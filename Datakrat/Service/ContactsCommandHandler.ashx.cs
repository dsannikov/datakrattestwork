﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using Datakrat.Core.DAL;
using Datakrat.Core.Models;
using Datakrat.Models;

namespace Datakrat.Service
{
    /// <summary>
    /// Summary description for ContactsCommandHandler
    /// </summary>
    public class ContactsCommandHandler : IHttpHandler
    {
        private const string GetContactsCommand = "getContacts";
        private const string SaveContactCommand = "saveContact";
        private const string RemoveContactCommand = "removeContact";
        private const string FindContactsCommand = "findContacts";
        private readonly List<string> _commands = new List<string>()
        {
            GetContactsCommand,
            SaveContactCommand,
            RemoveContactCommand,
            FindContactsCommand
        };
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.QueryString["cmd"] == null)
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.Response.ContentType = "text/plain";
                context.Response.Write("Параметер cmd не указан");
                return;
            }
            var command = context.Request.QueryString["cmd"];
            if (!_commands.Contains(command))
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.Response.ContentType = "text/plain";
                context.Response.Write("Параметер cmd указан неверно");
                return;
            }
            try
            {

                string result = "{}";
                switch (command)
                {
                    case GetContactsCommand:
                        {
                            result = HandleGetContacts();
                        }
                        break;
                    case SaveContactCommand:
                        {
                            string json = new StreamReader(context.Request.InputStream).ReadToEnd();
                            result = HandleSaveContact(json);
                        }
                        break;
                    case RemoveContactCommand:
                        {
                            string json = new StreamReader(context.Request.InputStream).ReadToEnd();
                            HandleRemoveContact(json);
                        }
                        break;
                    case FindContactsCommand:
                        {
                            string json = new StreamReader(context.Request.InputStream).ReadToEnd();
                            result = HandleFindContacts(json);
                        }
                        break;
                }
                context.Response.ContentType = "application/json";
                context.Response.Write(result);
                return;
            }
            catch (Exception ex)
            {
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                context.Response.ContentType = "text/plain";
                context.Response.Write("На сервере произошла ошибка. " + ex.Message);
            }

        }

        private string HandleGetContacts()
        {
            var contacts = ContactsDataService.Instance.GetAllContacts();
            return Newtonsoft.Json.JsonConvert.SerializeObject(contacts);
        }

        private string HandleSaveContact(string json)
        {
            var contact = Newtonsoft.Json.JsonConvert.DeserializeObject<SaveContactData>(json);
            var baseContact = contact.Id.HasValue ? ContactsDataService.Instance.UpdateContact(contact.Id.Value, contact.Name, contact.Phone, contact.Email, contact.Notes)
                                                  : ContactsDataService.Instance.AddContact(contact.Name, contact.Phone, contact.Email, contact.Notes);
            return Newtonsoft.Json.JsonConvert.SerializeObject(baseContact);
        }

        private void HandleRemoveContact(string json)
        {
            var contact = Newtonsoft.Json.JsonConvert.DeserializeObject<RemoveContactData>(json);
            if (contact.Id.HasValue)
            {
                ContactsDataService.Instance.RemoveContactById(contact.Id.Value);
            }
        }
        private string HandleFindContacts(string json)
        {
            var searchObject = Newtonsoft.Json.JsonConvert.DeserializeObject<FindContactsData>(json);

            return Newtonsoft.Json.JsonConvert.SerializeObject(ContactsDataService.Instance.FindContacts(searchObject.SearchText));

        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}