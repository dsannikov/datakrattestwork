﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Datakrat.Models
{
    public class FindContactsData
    {
        [JsonProperty("searchText")]
        public string SearchText { get; set; }
    }
}