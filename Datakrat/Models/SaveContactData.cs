﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Datakrat.Models
{
    public class SaveContactData
    {
        [JsonProperty("contactId")]
        public Guid? Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("phone")]
        public string Phone { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("notes")]
        public string Notes { get; set; }

    }
}