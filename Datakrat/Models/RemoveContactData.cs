﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Datakrat.Models
{
    public class RemoveContactData
    {
        [JsonProperty("contactId")]
        public Guid? Id { get; set; }
    }
}