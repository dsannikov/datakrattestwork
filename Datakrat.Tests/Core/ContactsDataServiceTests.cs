﻿using System;
using Datakrat.Core.DAL;
using Datakrat.Core.Exceptions;
using NUnit.Framework;

namespace Datakrat.Tests.Core
{
    [TestFixture]
    public class ContactsDataServiceTests
    {
        [SetUp]
        public void Init()
        {
            ContactsDataService.Instance.Init();
        }

        [Test]
        public void AddContactTest()
        {
            var contactsLength = ContactsDataService.Instance.GetAllContacts().Count;
            ContactsDataService.Instance.AddContact("TestContact", "TestContact", "TestContact", "TestContact");
            var afterContactsCount = ContactsDataService.Instance.GetAllContacts().Count;
            Assert.AreEqual(contactsLength + 1, afterContactsCount);
        }
        [Test]
        public void GetContactByIdTest()
        {
            var instance = ContactsDataService.Instance;
            var contact = instance.AddContact("TestContact", "TestContact", "TestContact", "TestContact");
            var id = contact.Id;
            var contactGet = instance.GetContactById(id);
            Assert.AreNotEqual(null, contactGet);
            Assert.AreEqual(id, contactGet.Id);
        }
        [Test]
        [ExpectedException(typeof(ContactNotFoundException))]
        public void GetContactByIdFailTest()
        {
            var instance = ContactsDataService.Instance;
            instance.GetContactById(Guid.NewGuid());
        }
        [Test]
        [ExpectedException(typeof(ContactNotFoundException))]
        public void RemoveContactByIdTest()
        {
            var instance = ContactsDataService.Instance;
            var contact = instance.AddContact("TestContact", "TestContact", "TestContact", "TestContact");
            var id = contact.Id;
            instance.RemoveContactById(id);
            instance.GetContactById(id);
        }
        [Test]
        public void FindContactsTest()
        {
            var instance = ContactsDataService.Instance;
            for (int i = 0; i <= 10; i++)
            {
                instance.AddContact("Контакт " + i + "searchString", "Телефон " + i, "Email " + i, "Notes " + i);
            }
            var contacts = instance.FindContacts("0searchString");
            Assert.AreEqual(contacts.Count, 2);
        }
        [Test]
        public void UpdateContactTest()
        {
            var instance = ContactsDataService.Instance;
            var contact = instance.AddContact("TestContact", "TestContact", "TestContact", "TestContact");
            var id = contact.Id;
            instance.UpdateContact(id, "TestContactNewName", "TestContactNewPhone", "TestContactNewEmail", "TestContactNewNotes");
            var newContact = instance.GetContactById(id);
            Assert.AreEqual(newContact.Name, "TestContactNewName");
            Assert.AreEqual(newContact.Phone, "TestContactNewPhone");
            Assert.AreEqual(newContact.Email, "TestContactNewEmail");
            Assert.AreEqual(newContact.Notes, "TestContactNewNotes");
        }
    }
}
