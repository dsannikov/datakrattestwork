﻿using System.IO;
using System.Net;
using System.Web;
using Datakrat.Service;
using NUnit.Framework;

namespace Datakrat.Tests.Site
{
    [TestFixture]
    public class CommandHandlerTests
    {
        [Test]
        public void TestWrongCommand()
        {
            using (MemoryStream memoryStream = new MemoryStream())
            using (TextWriter tw = new StreamWriter(memoryStream))
            {
                HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org", "cmd=adsasd"), new HttpResponse(tw));
                ContactsCommandHandler handler = new ContactsCommandHandler();
                handler.ProcessRequest(HttpContext.Current);
                Assert.AreEqual(HttpContext.Current.Response.StatusCode, (int)HttpStatusCode.BadRequest);
            }
        }
    }
}
