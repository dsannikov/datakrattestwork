﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datakrat.Core.Models
{
    public class Contact
    {
        public Guid Id { get; set; }
        public DateTimeOffset CreationDateTime { get; set; }
        public DateTimeOffset LastModifyDateTime { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Notes { get; set; }

        public string CreationDateTimeDisplay
        {
            get { return CreationDateTime.ToString("g"); }
        }

        public Contact(string name, string phone = "", string email = "", string notes = "")
        {
            Id = Guid.NewGuid();
            LastModifyDateTime = CreationDateTime = DateTimeOffset.Now;
            Name = name;
            Phone = phone;
            Email = email;
            Notes = notes;
        }

        public void Update(string name, string phone, string email, string notes)
        {
            LastModifyDateTime = DateTimeOffset.Now;
            Name = name;
            Phone = phone;
            Email = email;
            Notes = notes;
        }
    }
}
