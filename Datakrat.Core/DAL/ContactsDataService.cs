﻿using System;
using System.Collections.Generic;
using System.Linq;
using Datakrat.Core.Exceptions;
using Datakrat.Core.Models;

namespace Datakrat.Core.DAL
{
    public class ContactsDataService
    {
        #region Singleton
        private static volatile ContactsDataService _instance;
        private static readonly object SyncRoot = new object();
        public static ContactsDataService Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                            _instance = new ContactsDataService();
                    }
                }

                return _instance;
            }
        }
        #endregion

        private readonly List<Contact> _contacts;
        public ContactsDataService()
        {
            _contacts = new List<Contact>();
        }

        public void Init()
        {
//            for (int i = 0; i < 10; i++)
//            {
//                AddContact("Контакт " + i, "Телефон " + i, "Email " + i, "Notes " + i);
//            }
        }

        public Contact AddContact(string name, string phone, string email, string notes)
        {
            Contact contact = new Contact(name, phone, email, notes);
            _contacts.Add(contact);
            return contact;
        }
        public Contact GetContactById(Guid id)
        {
            var contact = _contacts.FirstOrDefault(c => c.Id == id);
            if (contact == null)
            {
                throw new ContactNotFoundException("Контакт с id: " + id + " не найден");
            }
            return contact;
        }
        public void RemoveContactById(Guid id)
        {
            var contact = GetContactById(id);
            _contacts.Remove(contact);
        }
        public List<Contact> FindContacts(string testForSearch)
        {
            if (string.IsNullOrWhiteSpace(testForSearch))
            {
                return _contacts;
            }
            var lowTrimSearchText = testForSearch.Trim().ToLower();
            return
                _contacts.Where(
                    c =>
                        c.Name.ToLower().Contains(lowTrimSearchText) || c.Phone.ToLower().Contains(lowTrimSearchText) ||
                        c.Email.ToLower().Contains(lowTrimSearchText) || c.Notes.ToLower().Contains(lowTrimSearchText) || 
                        c.CreationDateTimeDisplay.ToLower().Contains(lowTrimSearchText)).ToList();
        }
        public List<Contact> GetAllContacts()
        {
            return _contacts;
        }

        public Contact UpdateContact(Guid id, string name, string phone, string email, string notes)
        {
            var contact = GetContactById(id);
            contact.Update(name, phone, email, notes);
            return contact;
        }
    }
}
